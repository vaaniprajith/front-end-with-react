import React,{ Component } from 'react';
import { Card,CardImg,CardText,CardBody, CardTitle, Breadcrumb, BreadcrumbItem,Button,Modal, ModalHeader, ModalBody,Row, Col, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Control, LocalForm, Errors} from 'react-redux-form';
import { Loading } from './LoadingComponent';

	
const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

	class CommentForm extends Component{
		constructor(props){
			super(props);

			this.state={
				isModelOpen:false
			};
			this.toggleModal=this.toggleModal.bind(this);
			this.handleSubmit = this.handleSubmit.bind(this);
		}

		handleSubmit(values){
			this.props.addComment(this.props.dishId, values.rating, values.author, values.comment);
        //event.preventDefault();//prevent from moving to another page
    	}

		toggleModal(){
			this.setState({
				isModalOpen:!this.state.isModalOpen
			});
		}
		render(){
			return(
				<React.Fragment>
					<Button outline onClick={this.toggleModal}><span className="fa fa-pencil fa-lg"></span> Submit Comment</Button>
					<Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
						<ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
						<ModalBody>
							<LocalForm onSubmit={(values) => this.handleSubmit(values)}>
								<div>
									<Row className="form-group">
										<Label htmlFor="rating" md={12}>Rating</Label>
										<Col md={12}>
											<Control.select model=".rating" id="rating" name="rating"
		                                 	className="form-control">
			                                    <option>1</option>
			                                    <option>2</option>
			                                    <option>3</option>
			                                    <option>4</option>
			                                    <option>5</option>
		                                	</Control.select>
										</Col>	                                
									</Row>
									<Row className="form-group">
										<Label htmlFor="name" md={12}>Your Name</Label>
										<Col md={12}>
											<Control.text model=".name" 
												id="name" 
												name="name" 
												placeholder="Your Name"
												className="form-control"
												validators={{
                                            		required, minLength: minLength(3), maxLength: maxLength(15)
                                        		}}
                                        	/>
											<Errors
											  className="text-danger"
											  model=".name"
											  show="touched"
											  messages={{
											  	minLength: 'Must be greater than 2 characters.',
                                            	maxLength: 'Must  be 15 characters or less.'
											  }}
											/>
										</Col>
									</Row>
									<Row className="form-group">
										<Label htmlFor="formcomment" md={12}>Comment</Label>
										<Col md={12}>
											<Control.textarea model=".comment" 
												id="formcomment" 
												name="formcomment" 
												rows="6"
												className="form-control"/>
										</Col>
									</Row>
									<Row>
										<Col md={{size:10}}>
                                    <Button type="submit" color="primary">
                                    Submit
                                    </Button>
                                </Col>
									</Row>
								</div>
							</LocalForm>
						</ModalBody>
					</Modal>
				</React.Fragment>
				);
		}
	}
	function RenderDish({dish}){
		return(
				<Card>
					<CardImg width="100%" src={dish.image} alt={dish.name}/>
					<CardBody>
						<CardTitle>{dish.name}</CardTitle>
						<CardText>{dish.description}</CardText>
					</CardBody>
				</Card>
			);

	}

	function RenderComments({comments,addComment,dishId}){
		var commentList = comments.map((comment)=>{
			return(
			<li key={comment.id}>
				{comment.comment}
				<br/>
				<br/>
				--{comment.author},&nbsp;
				{new Intl.DateTimeFormat("EN", {
              day: "2-digit",
              month: "long",
              year: "numeric"
            }).format(new Date(comment.date))}
				<br/>
				<br/>
			</li>
			);

		})
		
		return (
		
        	<div>
                <h4>Comments</h4>
                <ul className="list-unstyled">
                    {commentList}
                    <CommentForm dishId={dishId} addComment={addComment} />
                </ul>
            </div>
        
        );
		
	}

	const DishDetail = (props) => {
		if(props.isLoading){
			return(
				<div className="container">
					<div className="row">
						<Loading />
					</div>
				</div>
			);
		}
		else if (props.errMess) {
			return(
				<div className="container">
					<div className="row">
						<h4>{props.errMess}</h4>
					</div>
				</div>
			);
		}
		if(props.dish!=null){
			return(
				<div className="container">
					<div className="row">
	                    <Breadcrumb>
	                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
	                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
	                    </Breadcrumb>
	                    <div className="col-12">
	                        <h3>{props.dish.name}</h3>
	                        <hr />
	                    </div>                
                	</div>
                	<div className="row">
						<RenderDish dish={props.dish}/>
						<RenderComments 
							comments = {props.comments}
							addComment={props.addComment}
        					dishId={props.dish.id}/>
						
					</div>
				</div>
				
				);
		}
		else{
			return(
			<div>
			</div>);	
		}
	}

	

export default DishDetail;